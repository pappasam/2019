---
name: Casey Faist
talks:
- "PPB: Unbearably Fun Game Development"
---
Queen Pythonista and Mother of Snakes at Heroku, Casey has done a little bit
of everything, from data engineering to AST code parsing, and is currently
building the next generation deploy experience for Python in the cloud. She
likes hugs, sharks and thinks combining those things would be awesome when
done very carefully.
