---
name: James Powell
talks:
- "Why should I write code when I can write code that writes code?"
---
James Powell is a professional Python programmer and enthusiast. He started
working with Python in the finance industry building reporting and analysis
systems for prop trading front offices. He currently works as a consultant
building data engineering and scientific computing platforms for a wide-
range of clients using cutting-edge open source tools like Python and React.
He also offers a variety of corporate training courses and coaching sessions
in all areas of Python.

In his spare time, he serves on the board of NumFOCUS as co-Chairman and
Vice President. NumFOCUS is the 501(c)(3) non-profit that supports all the
major tools in the Python data analysis ecosystem (incl. pandas, numpy,
jupyter, matplotlib, and others.) At NumFOCUS, he helps build global open
source communities for data scientists, data engineers, and business
analysis. He helps NumFOCUS run the PyData conference series and has sat on
speaker selection and organizing committees for over two dozen conferences.
James is also a prolific speaker: since 2013, he has given over seventy
conference talks at over fifty Python events worldwide.
